const express = require('express')

module.exports = function (server) {
  // API Routes
  const router = express.Router()
  server.use('/api', router)

  // myApp Routes
  const myappService = require('../api/myapp/myappService')
  myappService.register(router, '/myapp')
}