const myApp = require('./myapp')

myApp.methods(['get', 'post', 'put', 'delete'])
myApp.updateOptions({ new: true, runValidators: true })

module.exports = myApp