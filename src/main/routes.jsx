import React from 'react'
import { HashRouter, Route, Switch, Link } from 'react-router-dom'

import Todo from '../todo/todo'
import About from '../about/about'

export default props => (
  <div className='area-main'>
    <HashRouter>
      <Switch>
        <Route exact={true} path="/" component={Todo} />
        <Route path="/todas" component={Todo} />
        <Route path="/sobre" component={About} />
      </Switch>
    </HashRouter>
  </div>
)