import React from 'react'
import Row from '../template/row'
import Sidebar from '../template/sidebar'
import Content from '../template/content'

export default props => (
  <div>
    <Row align='stretch' justify='space-between'>
      <Sidebar />
      <Content />
    </Row>
  </div>
)

