import React, { Component } from 'react'
import axios from 'axios'

import Row from '../template/row'
import TodoForm from './todoForm'
import TodoList from './todoList'

const URL = 'http://localhost:3003/api/myapp'

export default class Todo extends Component {
  render() {
    return (
      <main>
        <Row>
          <h1>
            Lista de tarefas
          </h1>
        </Row>
        <TodoForm />
        <TodoList />
      </main>
    )
  }
}