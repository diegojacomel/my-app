import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { markAsDone, markAsPending, remove } from './todoActions'

const TodoList = props => {
  const renderRows = () => {
    const list = props.list || []
    return list.map((todo, index) =>(
      <tr key={index}>
        <td className={todo.done ? 'is-done' : ''}>
          {todo.description}
        </td>
        <td>
          <button className={`ui-button -success -${todo.done}`} onClick={() => props.markAsDone(todo)}>
            Feito
          </button>
          <button className={`ui-button -warning -${!todo.done}`} onClick={() => props.markAsPending(todo)}>
            Desfazer
          </button>
          <button className={`ui-button -danger -${!todo.done}`} onClick={() => props.remove(todo)}>
            Apagar
          </button>
        </td>
      </tr>
    ))
  }

  return (
    <table className='table' cellPadding='0' cellSpacing='0'>
      <thead>
        <tr>
          <th>Descrição</th>
          <th>Ações</th>
        </tr>
      </thead>
      <tbody>
        {renderRows()}
      </tbody>
    </table>
  )
}

const mapStateToProps = (state) => ({
  list: state.todo.list
})
const mapDispatchToProps = dispatch =>
  bindActionCreators({ markAsDone, markAsPending, remove }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(TodoList)