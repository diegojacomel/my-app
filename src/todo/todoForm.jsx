import React, { Component } from 'react'
import Row from '../template/row'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { add, changeDescription, search, clear } from './todoActions'

class TodoForm extends Component {
  constructor(props) {
    super(props)
    this.keyHandler = this.keyHandler.bind(this)
  }

  componentWillMount() {
    this.props.search()
  }

  keyHandler(e) {
    const { add, search, clear, description } = this.props
    if (e.key === 'Enter') {
      e.shiftKey ? search() : add(description)
    } else if (e.key === 'Escape') {
      clear()
    }
  }

  render() {
    const { add, search, clear, description } = this.props
    return (
      <div role='form' className='todoForm'>
        <Row align='center' justify='flex-end'>
          <input type="text"
            id='description'
            className='ui-input'
            placeholder='Digite aqui sua tarefa'
            onChange={this.props.changeDescription}
            onKeyUp={this.keyHandler}
            value={this.props.description} />
          <button className='ui-button' onClick={() => add(description)}>
            Adicionar tarefa
          </button>
          <button className='ui-button' onClick={search}>
            Pesquisar
          </button>
          <button className='ui-button' onClick={clear}>
            Limpar
          </button>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  description: state.todo.description
})

const mapDispatchToProps = dispatch => 
  bindActionCreators({ add, changeDescription, search, clear }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(TodoForm)