import React from 'react'
import TableItem from './tableItem'

export default props => (
  <table className="table" cellPadding='0' cellSpacing='0'>
    <thead>
      <tr>
        <th>
          Tarefa
        </th>
        <th>
          Status
        </th>
        <th>
          Açoes
        </th>
      </tr>
    </thead>
    <tbody>
      {props.children}
    </tbody>
  </table>
)