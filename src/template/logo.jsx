import React from 'react'
import Row from './row'

export default props => (
  <Row align='center' justify='center'>
    <a href={props.link} className='ui-logo'>
      {props.text}
    </a>
  </Row>
)