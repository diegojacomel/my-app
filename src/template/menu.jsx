import React from 'react'
import MenuItem from './menuItem'

export default props => (
  <ul className='ui-menu'>
    <MenuItem link='/todas' name='Todas' />
    <MenuItem link='/sobre' name='Sobre' />
  </ul>
)