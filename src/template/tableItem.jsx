import React from 'react'

export default props => (
  <tr>
    <td>{props.task}</td>
    <td>{props.status}</td>
    <td>{props.action}</td>
  </tr>
)