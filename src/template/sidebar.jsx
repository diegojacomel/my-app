import React from 'react'
import Logo from './logo'
import Menu from './menu'

export default props => (
  <aside className='sidebar-main'>
    <Logo link='#' text='ToDo App' />
    <Menu />
  </aside>
)