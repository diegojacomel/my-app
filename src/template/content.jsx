import React from 'react'
import Header from './header'
import Routes from '../main/routes'

export default props => (
  <div className="content-main">
    <Header />
    <Routes />
  </div>
)