import React from 'react'

export default props => (
  <div className={'flex' + (props.align ? (' align-' + props.align) : '') + (props.justify ? (' justify-' + props.justify) : '')}>
    {props.children}
  </div>
)