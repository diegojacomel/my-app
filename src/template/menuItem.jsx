import React from 'react'
import { HashRouter, NavLink } from 'react-router-dom'

export default props => (
  <HashRouter>
    <li>
      <NavLink to={props.link} activeClassName='is-active'>
        {props.name}
      </NavLink>
    </li>
  </HashRouter>
)