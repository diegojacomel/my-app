const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const nib = require('nib')
const rupture = require('rupture')

module.exports = {
  entry: './src/index.jsx',
  output: {
    path: __dirname + '/public',
    filename: './bundle.js'
  },
  devServer: {
    contentBase: __dirname + '/public'
  },
  resolve: {
    extensions: [' ', '.js', '.jsx']
  },
  module: {
    rules: [
      {
        test: /\.jsx?/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015', 'react'],
            plugins: ['transform-object-rest-spread']
          }
        }
      },
      {
        test: /\.styl$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            'css-loader',
            'stylus-loader'
          ],
        })
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('styles.css'),
    new webpack.LoaderOptionsPlugin({
      options: {
        stylus: {
          use: [
            nib(),
            rupture()
          ]
        }
      }
    })
  ]
}